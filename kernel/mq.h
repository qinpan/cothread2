/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef MQ_H_
#define MQ_H_

struct mq_wait {
    ccb_t *thread;
    struct mq_wait *prev;
    struct mq_wait *next;
};

struct mq_msg {
    int length;
    char *buff;
    struct mq_msg *next;
};

#define MQ_NAME_SIZE (16)

struct mq_des
{
    char name[MQ_NAME_SIZE];
    int maxmsgs;
    int max_permsg_bytes;
    int max_total_bytes;
    
    struct mq_msg *msg_head;
    struct mq_msg *msg_tail;
    struct mq_wait *wait_quene;
    
    int current_msgs;
    int current_bytes;
};

typedef struct mq_des *mqd_t;

typedef unsigned int mode_t;

mqd_t mq_open(const char *mq_name, int maxmsgs, int max_permsg_bytes, int max_total_bytes);
int __mq_receive(mqd_t mqdes, char *msg, int msglen, unsigned int *prio);
int mq_send(mqd_t mqdes, const char *msg, int msglen, unsigned int prio);
int mq_load_warning(mqd_t mqdes);

#define mq_receive(mqdes, msg, msglen, prio)\
while (__mq_receive(mqdes, msg, msglen, prio) <= 0)\
{\
    thread_wait(OsEvent(EVENT_MQ_ARRIVE), 0);\
    OsEventClr(EVENT_MQ_ARRIVE);\
    continue;\
}

#endif

